<!doctype html>
<html class="no-js " lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<title>Home</title>
<!-- <link rel="icon" href="favicon.ico" type="image/x-icon"> --> <!-- Favicon-->
<link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="plugins/morrisjs/morris.css" />
<link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-2.0.3.min.css"/>
<!-- Custom Css -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>

<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="css/color_skins.css">
<link rel="stylesheet" href="plugins/toastr/toastr.min.css">
<style>

/* Nav Meu Container */
#nav-menu-container {
  float: right;
  margin: 0;
}

@media (max-width: 768px) {
  #nav-menu-container {
    display: none;
  }
}

/* Nav Meu Styling */
.nav-menu a {
  padding: 10px 8px;
  text-decoration: none;
  display: inline-block;
  color: #fbfdfc;
  font-family: "Raleway", sans-serif;
  font-weight: 700;
  font-size: 14px;
  outline: none;
}

.nav-menu li:hover > a, .nav-menu > .menu-active > a {
  color: #50d8af;
}

.nav-menu > li {
  margin-left: 10px;
}

.nav-menu ul {
  margin: 4px 0 0 0;
  padding: 10px;
  box-shadow: 0px 0px 30px rgba(127, 137, 161, 0.25);
  background: #fff;
}

.nav-menu ul li {
  transition: 0.3s;
}

.nav-menu ul li a {
  padding: 10px;
  color: #333;
  transition: 0.3s;
  display: block;
  font-size: 13px;
  text-transform: none;
}

.nav-menu ul li:hover > a {
  color: #50d8af;
}

.nav-menu ul ul {
  margin: 0;
}

/* Mobile Nav Toggle */
#mobile-nav-toggle {
  position: fixed;
  right: 0;
  top: 0;
  z-index: 999;
  margin: 20px 20px 0 0;
  border: 0;
  background: none;
  font-size: 24px;
  display: none;
  transition: all 0.4s;
  outline: none;
  cursor: pointer;
}

#mobile-nav-toggle i {
  color: #555;
}

@media (max-width: 768px) {
  #mobile-nav-toggle {
    display: inline;
  }
}

/* Mobile Nav Styling */
#mobile-nav {
  position: fixed;
  top: 0;
  padding-top: 18px;
  bottom: 0;
  z-index: 998;
  background: rgba(52, 59, 64, 0.9);
  left: -260px;
  width: 260px;
  overflow-y: auto;
  transition: 0.4s;
}

#mobile-nav ul {
  padding: 0;
  margin: 0;
  list-style: none;
}

#mobile-nav ul li {
  position: relative;
}

#mobile-nav ul li a {
  color: #fff;
  font-size: 16px;
  overflow: hidden;
  padding: 10px 22px 10px 15px;
  position: relative;
  text-decoration: none;
  width: 100%;
  display: block;
  outline: none;
}

#mobile-nav ul li a:hover {
  color: #fff;
}

#mobile-nav ul li li {
  padding-left: 30px;
}

#mobile-nav ul .menu-has-children i {
  position: absolute;
  right: 0;
  z-index: 99;
  padding: 15px;
  cursor: pointer;
  color: #fff;
}

#mobile-nav ul .menu-has-children i.fa-chevron-up {
  color: #50d8af;
}

#mobile-nav ul .menu-item-active {
  color: #50d8af;
}

#mobile-body-overly {
  width: 100%;
  height: 100%;
  z-index: 997;
  top: 0;
  left: 0;
  position: fixed;
  background: rgba(52, 59, 64, 0.9);
  display: none;
}

/*--------------------------------------------------------------
# Navigation Menu
--------------------------------------------------------------*/
/* Nav Menu Essentials */
.nav-menu, .nav-menu * {
  margin: 0;
  padding: 0;
  list-style: none;
}

.nav-menu ul {
  position: absolute;
  display: none;
  top: 100%;
  left: 0;
  z-index: 99;
}

.nav-menu li {
  position: relative;
  white-space: nowrap;
}

.nav-menu > li {
  float: left;
}

.nav-menu li:hover > ul,
.nav-menu li.sfHover > ul {
  display: block;
}

.nav-menu ul ul {
  top: 0;
  left: 100%;
}

.nav-menu ul li {
  min-width: 180px;
}

/* Nav Menu Arrows */
.sf-arrows .sf-with-ul {
  padding-right: 22px;
}

.sf-arrows .sf-with-ul:after {
  content: "\f107";
  position: absolute;
  right: 8px;
  font-family: FontAwesome;
  font-style: normal;
  font-weight: normal;
}

.sf-arrows ul .sf-with-ul:after {
  content: "\f105";
}

/* Nav Meu Container */
#nav-menu-container {
  float: right;
  margin: 0;
}

@media (max-width: 768px) {
  #nav-menu-container {
    display: none;
  }
}
/* Mobile Nav body classes */
body.mobile-nav-active {
  overflow: hidden;
}

body.mobile-nav-active #mobile-nav {
  left: 0;
}

body.mobile-nav-active #mobile-nav-toggle {
  color: #fff;
}
/*--------------------------------------------------------------
# Intro Section
--------------------------------------------------------------*/
#intro {
  width: 100%;
  height: 60vh;
  position: relative;
  /* background: url("../img/intro-carousel/1.jpg") no-repeat; */
  background-size: cover;
}

#intro .intro-content {
  position: absolute;
  bottom: 0;
  top: 0;
  left: 0;
  right: 0;
  z-index: 10;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  text-align: center;
}

#intro .intro-content h2 {
  color: #0c2e8a;
  margin-bottom: 30px;
  font-size: 64px;
  font-weight: 700;
}

#intro .intro-content h2 span {
  color: #50d8af;
  text-decoration: underline;
}

@media (max-width: 767px) {
  #intro .intro-content h2 {
    font-size: 34px;
  }
}

#intro .intro-content .btn-get-started, #intro .intro-content .btn-projects {
  font-family: "Raleway", sans-serif;
  font-size: 15px;
  font-weight: bold;
  letter-spacing: 1px;
  display: inline-block;
  padding: 10px 32px;
  border-radius: 2px;
  transition: 0.5s;
  margin: 10px;
  color: #fff;
}

#intro .intro-content .btn-get-started {
  background: #0c2e8a;
  border: 2px solid #0c2e8a;
}

#intro .intro-content .btn-get-started:hover {
  background: none;
  color: #0c2e8a;
}

#intro .intro-content .btn-projects {
  background: #50d8af;
  border: 2px solid #50d8af;
}

#intro .intro-content .btn-projects:hover {
  background: none;
  color: #50d8af;
}

#intro #intro-carousel {
  z-index: 8;
}

#intro #intro-carousel::before {
  content: '';
  background-color: rgba(255, 255, 255, 0.7);
  position: absolute;
  height: 100%;
  width: 100%;
  top: 0;
  right: 0;
  left: 0;
  bottom: 0;
  z-index: 7;
}

#intro #intro-carousel .item {
  width: 100%;
  height: 60vh;
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  -webkit-transition-property: opacity;
  transition-property: opacity;
}

</style>
</head>
<body>
<div id="header-sticky-wrapper" class="sticky-wrapper" style="height: 84px;">

    <header id="header" style="width: 1583px;">
    <div class="container">

      <div id="logo" class="pull-left">
        <h1><a href="#body" class="scrollto">Payroll</h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="#body"><img src="img/logo.png" alt="" title="" /></a>-->
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu sf-js-enabled sf-arrows" style="touch-action: pan-y;">
          <li class="menu-active"><a href="#body">Home</a></li>
          <li><a href="#about">About Us</a></li>
          <li><a href="#services">Services</a></li>
          <li><a href="#portfolio">Portfolio</a></li>
          <li><a href="#team">Team</a></li>
          <li class="menu-has-children"><a href="" class="sf-with-ul">Drop Down</a>
            <ul style="display: none;">
              <li><a href="#">Drop Down 1</a></li>
              <li><a href="#">Drop Down 3</a></li>
              <li><a href="#">Drop Down 4</a></li>
              <li><a href="#">Drop Down 5</a></li>
            </ul>
          </li>
          <div class="pull-right">
<button class="btn btn btn-primary" type="submit" style="padding: 0px;width: 180px;background: #6ad8b0;"> <a href="registertry">Free trail</a></button>

<button class="btn btn btn-primary" type="submit" style="padding: 0px;width: 180px;background: #1d2d89;"><a href="trylogin">Login</a></button>
</div>
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header></div>

<section id="intro">

    <div class="intro-content">
      <h2>Making <span>your business</span><br>happen easily!</h2>
      <div>
        <a href="pricing" class="btn-get-started scrollto">Buy Now</a>
        <a href="registertry" class="btn-projects scrollto">Try Demo</a>
      </div>
    </div>

  </section><!-- #intro -->
<!-- Jquery Core Js -->
<script src="bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js ( jquery.v3.2.1, Bootstrap4 js) -->
<script src="bundles/vendorscripts.bundle.js"></script> <!-- slimscroll, waves Scripts Plugin Js -->

<script src="bundles/knob.bundle.js"></script> <!-- Jquery Knob-->
<script src="bundles/jvectormap.bundle.js"></script> <!-- JVectorMap Plugin Js -->
<script src="bundles/morrisscripts.bundle.js"></script> <!-- Morris Plugin Js --> 
<script src="bundles/sparkline.bundle.js"></script> <!-- sparkline Plugin Js --> 
<script src="bundles/doughnut.bundle.js"></script>

<script src="bundles/mainscripts.bundle.js"></script>
<script src="js/pages/index.js"></script>
<script src="plugins/toastr/toastr.js"></script>

</body>

</html>