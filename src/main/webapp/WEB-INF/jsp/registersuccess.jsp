<%-- 
    Document   : registersuccess
    Created on : 14 Aug, 2018, 10:14:23 AM
    Author     : Burgeonits
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
     <title>HR Tool</title><meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="css/bootstrap.min.css" />
<!--    <link rel="stylesheet" href="css/bootstrap-responsive.min.css" /> -->
        <link rel="stylesheet" href="css/maruti-login.css" />
    </head>
    <body style="background-color: #ffff;">
        <style>
   .x-header {
    background-image: none;
}
.x-header {
    background: #00a3d3;
}
.x-header {
    color: #fff;
    display: block;
    background-color: #0590c3;
 
    background: -webkit-gradient(linear,50% 0,50% 100%,color-stop(0%,#1696c5),color-stop(100%,#048bbe));
    background: -moz-linear-gradient(#1696c5,#048bbe);
    background: -webkit-linear-gradient(#1696c5,#048bbe);
    background: linear-gradient(#1696c5,#048bbe);
}

        </style>
        <div class="x-header">
            <div class="inner">
                    <h1><span>HR Tool</span></h1>
            </div>
        </div>
        <div class="container login-container">
           <svg class="svg-icon" viewBox="0 0 20 20">
                            <path d="M17.388,4.751H2.613c-0.213,0-0.389,0.175-0.389,0.389v9.72c0,0.216,0.175,0.389,0.389,0.389h14.775c0.214,0,0.389-0.173,0.389-0.389v-9.72C17.776,4.926,17.602,4.751,17.388,4.751 M16.448,5.53L10,11.984L3.552,5.53H16.448zM3.002,6.081l3.921,3.925l-3.921,3.925V6.081z M3.56,14.471l3.914-3.916l2.253,2.253c0.153,0.153,0.395,0.153,0.548,0l2.253-2.253l3.913,3.916H3.56z M16.999,13.931l-3.921-3.925l3.921-3.925V13.931z"></path>
                        </svg>
            <div class="row">

                <div class="message-dynamic text">
                        <h1 class="title title-dynamic">${Firstname}, an email is on its way</h1>
                        <p class="email-dynamic">We sent ${Email} it to  and need to make sure it reached you</p>
                         <!--<p class="email-dynamic">We sent ${Temp} it to  and need to make sure it reached you</p>-->
                    </div>




  
   <c:if test="${Temp == 'adriott.com'}">   
        <div class="text-center">
         <p class="email-dynamic">We sent ${Temp} it to  and need to make sure it reached you</p>
         <a class="btn btn-primary lgn-btn2" href="https://www.adriott.com:2096/">Go to inbox</a>
<!--                <a class="btn btn-primary lgn-btn2" href="https://www.gmail.com" target="_blank">Go to inbox</a>  -->
      
      </div>          
    </c:if>
      <c:if test="${Temp == 'gmail.com'}">   
        <div class="text-center">
         <p class="email-dynamic">We sent ${Temp} it to  and need to make sure it reached you</p>
         <a class="btn btn-primary lgn-btn2" href="https://www.gmail.com">Go to inbox</a>
<!--                <a class="btn btn-primary lgn-btn2" href="https://www.gmail.com" target="_blank">Go to inbox</a>  -->
      
      </div>  
     </c:if>   
        <c:if test="${Temp == 'yahoo.com'}">   
        <div class="text-center">
         <p class="email-dynamic">We sent ${Temp} it to  and need to make sure it reached you</p>
         <a class="btn btn-primary lgn-btn2" href="https://login.yahoo.com/">Go to inbox</a>
<!--                <a class="btn btn-primary lgn-btn2" href="https://www.gmail.com" target="_blank">Go to inbox</a>  -->
      
      </div>  
     </c:if>    
                  
<!--       <div class="text-center">   
         
          <a class="btn btn-primary lgn-btn2" href="https://www.adriott.com:2096/ target="_blank">Go to inbox</a>
          
        </div>-->
        
                    


        <script src="js/jquery.min.js"></script>  
        <script src="js/maruti.login.js"></script> 
    </body>

</html>

</html>
