<!doctype html>
<html class="no-js " lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<title>Home</title>
<!-- <link rel="icon" href="favicon.ico" type="image/x-icon"> --> <!-- Favicon-->
<link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="plugins/morrisjs/morris.css" />
<link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-2.0.3.min.css"/>
<!-- Custom Css -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>

<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="css/color_skins.css">
<link rel="stylesheet" href="plugins/toastr/toastr.min.css">

</head>
<body class="theme-black">
<!-- Page Loader -->
<!-- <div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img src="assets/images/logo.svg" width="48" height="48" alt="Alpino"></div>
        <p>Please wait...</p>        
    </div>
</div> -->
<div class="overlay_menu">
    <button class="btn btn-primary btn-icon btn-icon-mini btn-round"><i class="zmdi zmdi-close"></i></button>
    <div class="container">        
        <div class="row clearfix">
            <div class="card">
                <div class="body">
                    <div class="input-group mb-0">
                        <input type="text" class="form-control" placeholder="Search...">
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="zmdi zmdi-search"></i></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card links">
                <div class="body">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-sm-12">
                            <h6>App</h6>
                            <ul class="list-unstyled">
                                <li><a href="">Inbox</a></li>
                                <li><a href="">Chat</a></li>
                                <li><a href="">Calendar</a></li>
                                <li><a href="">File Manager</a></li>
                                <li><a href="contact.html">Contact list</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12">
                            <h6>User Interface (UI)</h6>
                            <ul class="list-unstyled">
                                <li><a href="">UI KIT</a></li>
                                <li><a href="">Tabs</a></li>
                                <li><a href="">Range Sliders</a></li>
                                <li><a href="">Modals</a></li>
                                <li><a href="">Alerts</a></li>
                                <li><a href="">Dialogs</a></li>
                                <li><a href="">Collapse</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12">
                            <h6>Sample Pages</h6>
                            <ul class="list-unstyled">                            
                                <li><a href="">Image Gallery</a></li>
                                <li><a href="">Profile</a></li>
                                <li><a href="">Timeline</a></li>
                                <li><a href="">Pricing</a></li>
                                <li><a href="">Invoices</a></li>
                                <li><a href="">FAQs</a></li>
                                <li><a href="">Search Results</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12">
                            <h6>About</h6>
                            <ul class="list-unstyled">
                                <li><a href="" target="_blank">About</a></li>
                                <li><a href="" target="_blank">Contact Us</a></li>
                                <li><a href="" target="_blank">Admin Templates</a></li>
                                <li><a href="" target="_blank">Services</a></li>
                                <li><a href="" target="_blank">Portfolio</a></li>                            
                            </ul>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12">
        
            </div>
        </div>
    </div>
</div>
<div class="overlay"></div><!-- Overlay For Sidebars -->

<!-- Left Sidebar -->
<aside id="minileftbar" class="minileftbar">
    <ul class="menu_list">
        <li>
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" href="index.html"><img src="images/logo.svg" alt="Alpino"></a>
        </li>
        <li><a href="javascript:void(0);" class="btn_overlay hidden-sm-down"><i class="zmdi zmdi-search"></i></a></li>        
       <!--  <li><a href="javascript:void(0);" class="menu-sm"><i class="zmdi zmdi-swap"></i></a></li>      -->   
        <li class="menuapp-btn"><a href=""><i class="zmdi zmdi-apps"></i></a></li>
        <li class="notifications badgebit">
            <a href="javascript:void(0);">
                <i class="zmdi zmdi-notifications"></i>
                <div class="notify">
                    <span class="heartbit"></span>
                    <span class="point"></span>
                </div>
            </a>
        </li>
        <li class="task badgebit">
            <a href="javascript:void(0);">
                <i class="zmdi zmdi-account-box-phone"></i>
                <div class="notify">
                    <span class="heartbit"></span>
                    <span class="point"></span>
                </div>
            </a>
        </li>
        <li><a href="events.html" title="Events"><i class="zmdi zmdi-calendar"></i></a></li>
        <!-- <li><a href="mail-inbox.html" title="Inbox"><i class="zmdi zmdi-email"></i></a></li> -->
        <li><a href="" title="Contact List"><i class="zmdi zmdi-account-box-phone"></i></a></li>        
        <li><a href=""><i class="zmdi zmdi-comments"></i></a></li>        
        <li><a href="javascript:void(0);" class="fullscreen" data-provide="fullscreen"><i class="zmdi zmdi-fullscreen"></i></a></li>
        <li class="power">
            <a href="javascript:void(0);" class="js-right-sidebar"><i class="zmdi zmdi-settings zmdi-hc-spin"></i></a>            
            <a href="sign-in.html" class="mega-menu"><i class="zmdi zmdi-power"></i></a>
        </li>
    </ul>    
</aside>

<aside class="right_menu">
    <div class="menu-app">
        <div class="slim_scroll">
            <div class="card">
                <div class="header">
                    <h2><strong>App</strong> Menu</h2>
                </div>
                <div class="body">
                    <ul class="list-unstyled menu">
                        <li><a href=""><i class="zmdi zmdi-calendar-note"></i><span>Calendar</span></a></li>
                        <li><a href=""><i class="zmdi zmdi-file-text"></i><span>File Manager</span></a></li>
                        <li><a href=""><i class="zmdi zmdi-blogger"></i><span>Blog</span></a></li>
                        <li><a href="javascript:void(0)"><i class="zmdi zmdi-arrows"></i><span>Notes</span></a></li>
                        <li><a href="javascript:void(0)"><i class="zmdi zmdi-view-column"></i><span>Taskboard</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="notif-menu">
        <div class="slim_scroll">
            <div class="card">
                <div class="header">
                    <h2><strong>Messages</strong></h2>
                </div>
                <div class="body">
                    <ul class="messages list-unstyled">
                        <li>
                            <a href="javascript:void(0);">
                                <div class="media">
                                    <img class="media-object rounded-circle" src="images/xs/avatar1.jpg" alt="">
                                    <div class="media-body">
                                        <span class="name">Alexander <small class="time">35min ago</small></span>
                                        <p class="message">New tasks needs to be done</p>                                        
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <div class="media">
                                    <img class="media-object rounded-circle" src="images/xs/avatar2.jpg" alt="">
                                    <div class="media-body">
                                        <span class="name">Grayson <small class="time">1hr ago</small></span>
                                        <p class="message">New tasks needs to be done</p>                                        
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <div class="media">
                                    <img class="media-object rounded-circle" src="images/xs/avatar3.jpg" alt="">
                                    <div class="media-body">
                                        <span class="name">Sophia <small class="time">31min ago</small></span>
                                        <p class="message">New tasks needs to be done</p>                                        
                                    </div>
                                </div>
                            </a>
                        </li>        
                    </ul>  
                </div>
            </div>
            <div class="card">
                <div class="header">
                    <h2><strong>Notifications</strong></h2>
                </div>
                <div class="body">
                    <ul class="notification list-unstyled">
                        <li>
                            <i class="zmdi zmdi-balance-wallet text-success"></i>
                            <strong>+$30 New sale</strong>
                            <p><a href="javascript:void(0)">Admin Template</a></p>
                            <small class="text-muted">7 min ago</small>
                        </li>
                        <li>
                            <i class="zmdi zmdi-edit text-info"></i>
                            <strong>You Edited file</strong>
                            <p><a href="javascript:void(0)">Docs.doc</a></p>
                            <small class="text-muted">15 min ago</small>
                        </li>
                        <li>
                            <i class="zmdi zmdi-delete text-danger"></i>
                            <strong>Project removed</strong>
                            <p><a href="javascript:void(0)">AdminX BS4</a></p>
                            <small class="text-muted">1 hours ago</small>
                        </li>
                        <li>
                            <i class="zmdi zmdi-account text-success"></i>
                            <strong>New user</strong>
                            <p><a href="javascript:void(0)">UI Designer</a></p>
                            <small class="text-muted">1 hours ago</small>
                        </li>
                        <li>
                            <i class="zmdi zmdi-flag text-warning"></i>
                            <strong>Alpino v1.0.0 is available</strong>
                            <p><a href="javascript:void(0)">Update now</a></p>
                            <small class="text-muted">5 hours ago</small>
                        </li>
                    </ul>  
                </div>
            </div>
        </div>
    </div>
    <div class="task-menu">


               <div class="slim_scroll">
     <div class="card">
        <h2><strong>Employees</strong></h2>
                <div class="header">
                    <h2><strong>Main</strong></h2>
                </div>
                <div class="body">
                    <ul class="notification list-unstyled">
                        <li>
                             <i class="zmdi zmdi-balance-wallet text-success"></i>                           
                            <p><a href="">Overwiew</a></p>
                            
                        </li>

                        <li>
                            <i class="zmdi zmdi-account text-success"></i>                           
                            <p><a href="">Employee Directory</a></p>
                            
                        </li>

                    </ul>  
                </div>
            </div>
            <div class="card">
                <div class="header">
                    <h2><strong>Information</strong></h2>
                </div>
                <div class="body">
                    <ul class="notification list-unstyled">
                           <li>
                             <i class="zmdi zmdi-balance-wallet text-success"></i>                           
                            <p><a href="">Employee Profile</a></p>
                            
                        </li>
                           <li>
                             <i class="zmdi zmdi-balance-wallet text-success"></i>                           
                            <p><a href="">Bank/PF/ESI</a></p>
                            
                        </li>
                           <li>
                             <i class="zmdi zmdi-balance-wallet text-success"></i>                           
                            <p><a href="">Position History</a></p>
                            
                        </li>
                           <li>
                             <i class="zmdi zmdi-balance-wallet text-success"></i>                           
                            <p><a href="">Employee Documents</a></p>
                            
                        </li>
                           <li>
                             <i class="zmdi zmdi-balance-wallet text-success"></i>                           
                            <p><a href="">Employee Salary</a></p>
                            
                        </li>

                 
                    </ul>  
                </div>
            </div>
                        <div class="card">
                <div class="header">
                    <h2><strong>Setup</strong></h2>
                </div>
                <div class="body">
                    <ul class="notification list-unstyled">
                        <li>
                             <i class="zmdi zmdi-balance-wallet text-success"></i>                           
                            <p><a href="">HR Forms</a></p>
                            
                        </li>
                                             <li>
                             <i class="zmdi zmdi-balance-wallet text-success"></i>                           
                            <p><a href="">Letter Template</a></p>
                            
                        </li>
                    </ul>  
                </div>
            </div>
        </div>
        <div class="slim_scroll">
            <div class="card tasks">
                <div class="header">
                    <h2><strong>Project</strong> Status</h2>
                </div>
                
                                <ul>
                                        <li class="mid-level open">
                                            <a href="/ngapp/employee/main">
                                                <span class="menu_text">Main</span>
                                                <i class="icon-chevron-down"></i>
                                            </a>
                                                <ul>
                                                            <li class="leaf-level open">
                                                                <a href="/ngapp/employee/main/overview">
                                                                    <span class="menu_text">Overview</span>
                                                                </a>
                                                            </li>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/main/emp-directory">
                                                                    <span class="menu_text">Employee Directory</span>
                                                                </a>
                                                            </li>

                                                </ul>
                                        </li>
                                        <li class="mid-level">
                                            <a href="/ngapp/employee/info">
                                                <span class="menu_text">Information</span>
                                                <i class="icon-chevron-down"></i>
                                            </a>
                                                <ul>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/info/profile">
                                                                    <span class="menu_text">Employee Profile</span>
                                                                </a>
                                                            </li>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/info/accounts">
                                                                    <span class="menu_text">Bank/PF/ESI</span>
                                                                </a>
                                                            </li>

                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/info/assets">
                                                                    <span class="menu_text">Assets</span>
                                                                </a>
                                                            </li>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/info/career_history">
                                                                    <span class="menu_text">Position History</span>
                                                                </a>
                                                            </li>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/info/previousemployeement">
                                                                    <span class="menu_text">Previous Employment</span>
                                                                </a>
                                                            </li>

                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/info/employee-docs">
                                                                    <span class="menu_text">Employee Documents</span>
                                                                </a>
                                                            </li>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/info/employee-contract">
                                                                    <span class="menu_text">Employee Contracts</span>
                                                                </a>
                                                            </li>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/info/employeesalary">
                                                                    <span class="menu_text">Employee Salary</span>
                                                                </a>
                                                            </li>
                                                </ul>
                                        </li>

                                        <li class="mid-level">
                                            <a href="/ngapp/employee/setup">
                                                <span class="menu_text">Setup</span>
                                                <i class="icon-chevron-down"></i>
                                            </a>
                                                <ul>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/setup/letter-template">
                                                                    <span class="menu_text">Letter Template</span>
                                                                </a>
                                                            </li>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/setup/hr-forms">
                                                                    <span class="menu_text">HR Forms</span>
                                                                </a>
                                                            </li>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/setup/employeesegment">
                                                                    <span class="menu_text">Employee Segment</span>
                                                                </a>
                                                            </li>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/setup/v2/qb/employee-filter">
                                                                    <span class="menu_text">Employee Filter</span>
                                                                </a>
                                                            </li>
                                                </ul>
                                        </li>

                            </ul>
            </div>
        </div>



    </div>
    <div id="rightsidebar" class="right-sidebar">
        <ul class="nav nav-tabs">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#setting">Setting</a></li>        
           </ul>
        <div class="tab-content slim_scroll">
            <div class="tab-pane slideRight active" id="setting">
                <div class="card">
                    <div class="header">
                        <h2><strong>Colors</strong> Skins</h2>
                    </div>
                    <div class="body">
                        <ul class="choose-skin list-unstyled m-b-0">
                            <li data-theme="black" class="active">
                                <div class="black"></div>
                            </li>
                            <li data-theme="purple">
                                <div class="purple"></div>
                            </li>                   
                            <li data-theme="blue">
                                <div class="blue"></div>
                            </li>
                            <li data-theme="cyan">
                                <div class="cyan"></div>                    
                            </li>
                            <li data-theme="green">
                                <div class="green"></div>
                            </li>
                            <li data-theme="orange">
                                <div class="orange"></div>
                            </li>
                            <li data-theme="blush">
                                <div class="blush"></div>                    
                            </li>
                        </ul>
                    </div>
                </div>                
                
            </div>
          
        </div>
    </div>

 </aside>

<!-- Main Content -->
<section class="content home">
    <div class="container-fluid">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Dropdown
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#">Disabled</a>
      </li>
    </ul>
<form class="form-inline md-form form-sm">
    <i class="fa fa-search" aria-hidden="true"></i>
    <input class="form-control form-control-sm ml-3 w-75" type="text" placeholder="Search" aria-label="Search">
</form>
  </div>
</nav>
    </div>
</section>
<section class="content profile-page">
    <div class="container-fluid">

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="body bg-dark profile-header">
                        <div class="row">

                            <div class="col-lg-10 col-md-12">
                                <img src="images/logo.png" class="user_pic rounded img-raised" alt="User">
                                <div class="detail">
                                    <div class="u_name">
                                        <h4><strong>ABS</strong> Pvt Ltd</h4>
                                        <span>info@example.com</span>
                                        
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                    <ul class="nav nav-tabs profile_tab">
                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#overview">Overview</a></li>
                      <!--   <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#schedule">Schedule</a></li> -->
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#usersettings">Settings</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#addemployee">AddEmployee</a></li>
                    </ul>
                </div>
                <div class="tab-content container">
                    <div role="tabpanel" class="tab-pane active" id="overview">
                        <div class="row">
                            <div class="col-lg-3 col-md-6 col-6">
                                <div class="card">
                                   
                                    <div class="body">
                            <div class="icon"><i class="fa fa-user"></i> </div>
                            <div class="content">
                                <div class="text">New Employee</div>
                                   <h5 class="m-b-0 number count-to" data-from="0" data-to="10" data-speed="1000" data-fresh-interval="700">10</h5>
                            </div>
                            
                          
                        </div>

                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-6">
                                <div class="card">
                                    <div class="body">                            
                                          <div class="icon"><i class="fa fa-users"></i> </div>
                            <div class="content">
                                <div class="text">Total Employee</div>
                                <h5 class="m-b-0 number count-to" data-from="0" data-to="100" data-speed="1000" data-fresh-interval="700">100</h5>
                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-6">
                                <div class="card">
                                <div class="card">
                                    <div class="body">                            
                                          <div class="icon"><i class="fa fa-users"></i> </div>
                            <div class="content">
                                <div class="text">New Joinee</div>
                                <h5 class="m-b-0 number count-to" data-from="0" data-to="5" data-speed="1000" data-fresh-interval="700">5</h5>
                            </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-6">
                                <div class="card">
                                <div class="card">
                                    <div class="body">                            
                                          <div class="icon"><i class="fa fa-users"></i> </div>
                            <div class="content">
                                <div class="text">Total Clients</div>
                                <h5 class="m-b-0 number count-to" data-from="0" data-to="100" data-speed="1000" data-fresh-interval="700">100</h5>
                            </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-12">
                                <div class="card">
                                    <div class="header">
                                        <h2><strong>Info</strong></h2>
                                        <ul class="header-dropdown">
                                            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a href="javascript:void(0);">Action</a></li>
                                                    <li><a href="javascript:void(0);">Another action</a></li>
                                                    <li><a href="javascript:void(0);">Something else</a></li>
                                                    <li><a href="javascript:void(0);" class="boxs-close">Delete</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="body">
                                        <small class="text-muted">Address: </small>
                                        <p>Plot #:253, 2nd Floor, Srinivas Nagar,<br>Kapra, Hyderabad, TS, INDIA- 500062</p>
                                        <div>
                                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3805.611412150142!2d78.56076631496607!3d17.47830338802415!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bcb9b81cdf1782b%3A0x6a20aa2600c82edb!2sBurgeon+IT+Services!5e0!3m2!1sen!2sin!4v1535099491751" width="100%" height="150" class="border-0" allowfullscreen=""></iframe>

                                        </div>
                                        <hr>
                                        <small class="text-muted">Email address: </small>
                                        <p>burgeonits.com</p>
                                        <hr>
                                        <small class="text-muted">Phone: </small>
                                        <p>+91 40 27142939</p>
                                        <hr>
                                        <small class="text-muted">Established: </small>
                                        <p class="m-b-0">October 22th, 1990</p>
                                    </div>
                                </div>
                  
                            </div>
                         
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane page-calendar" id="schedule">
                        <div class="row">
                            <div class="col-md-12 col-lg-4">
                                <div class="card">
                                    <div class="body m-b-20">
                                        <div class="event-name b-lightred row">
                                            <div class="col-3 text-center">
                                                <h4>09<span>Dec</span><span>2017</span></h4>
                                            </div>
                                            <div class="col-9">
                                                <h6>Repeating Event</h6>
                                                <p>It is a long established fact that a reader will be distracted</p>
                                                <address><i class="zmdi zmdi-pin"></i> 123 6th St. Melbourne, FL 32904</address>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="event-name b-greensea row">
                                            <div class="col-3 text-center">
                                                <h4>16<span>Dec</span><span>2017</span></h4>
                                            </div>
                                            <div class="col-9">
                                                <h6>Repeating Event</h6>
                                                <p>It is a long established fact that a reader will be distracted</p>
                                                <address><i class="zmdi zmdi-pin"></i> 123 6th St. Melbourne, FL 32904</address>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="body m-b-20 l-blue">
                                        <div class="event-name row">
                                            <div class="col-3 text-center">
                                                <h4>28<span>Dec</span><span>2017</span></h4>
                                            </div>
                                            <div class="col-9">
                                                <h6>Google</h6>
                                                <p>It is a long established fact that a reader will be distracted</p>
                                                <address><i class="zmdi zmdi-pin"></i> 123 6th St. Melbourne, FL 32904</address>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="body m-b-5 l-green">
                                        <div class="event-name row">
                                            <div class="col-3 text-center">
                                                <h4>13<span>Dec</span><span>2017</span></h4>
                                            </div>
                                            <div class="col-9">
                                                <h6>Birthday</h6>
                                                <p>It is a long established fact that a reader will be distracted</p>
                                                <address><i class="zmdi zmdi-pin"></i> 123 6th St. Melbourne, FL 32904</address>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="body l-green">
                                        <div class="event-name row">
                                            <div class="col-3 text-center">
                                                <h4>11<span>Dec</span><span>2017</span></h4>
                                            </div>
                                            <div class="col-9">
                                                <h6>Conference</h6>
                                                <p>It is a long established fact that a reader will be distracted</p>
                                                <address><i class="zmdi zmdi-pin"></i> 123 6th St. Melbourne, FL 32904</address>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-round btn-primary m-t-15" data-toggle="modal" data-target="#addevent">Add Events</button>
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-8">
                                <div class="card">                                    
                                    <div class="body">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="usersettings">
                        <div class="card">
                            <div class="header">
                                <h2><strong>Security</strong> Settings</h2>
                            </div>
                            <div class="body">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Username">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" placeholder="Current Password">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" placeholder="New Password">
                                </div>
                                <button class="btn btn-info btn-round">Save Changes</button>                               
                            </div>
                        </div>
                        <div class="card">
                            <div class="header">
                                <h2><strong>Account</strong> Settings</h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="First Name">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Last Name">
                                        </div>
                                    </div>                                    
                                    <div class="col-lg-4 col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="City">
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="E-mail">
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Country">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <textarea rows="4" class="form-control no-resize" placeholder="Address Line 1"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="checkbox">
                                            <input id="procheck1" type="checkbox">
                                            <label for="procheck1">Profile Visibility For Everyone</label>
                                        </div>
                                        <div class="checkbox">
                                            <input id="procheck2" type="checkbox">
                                            <label for="procheck2">New task notifications</label>
                                        </div>
                                        <div class="checkbox">
                                            <input id="procheck3" type="checkbox">
                                            <label for="procheck3">New friend request notifications</label>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button class="btn btn-primary btn-round">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section>

</section>
<!-- Jquery Core Js -->
<script src="bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js ( jquery.v3.2.1, Bootstrap4 js) -->
<script src="bundles/vendorscripts.bundle.js"></script> <!-- slimscroll, waves Scripts Plugin Js -->

<script src="bundles/knob.bundle.js"></script> <!-- Jquery Knob-->
<script src="bundles/jvectormap.bundle.js"></script> <!-- JVectorMap Plugin Js -->
<script src="bundles/morrisscripts.bundle.js"></script> <!-- Morris Plugin Js --> 
<script src="bundles/sparkline.bundle.js"></script> <!-- sparkline Plugin Js --> 
<script src="bundles/doughnut.bundle.js"></script>

<script src="bundles/mainscripts.bundle.js"></script>
<script src="js/pages/index.js"></script>
<script src="plugins/toastr/toastr.js"></script>
<script>
$(function() {
   
    // notification popup
    toastr.options.closeButton = true;
    toastr.options.positionClass = 'toast-bottom-right';    
    toastr['success']('${Firstname},welcome to your dashboard');
});

</script>
</body>

</html>