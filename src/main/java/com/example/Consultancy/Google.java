/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.Consultancy;

import com.google.cloud.translate.Translate;
import com.google.cloud.translate.Translate.TranslateOption;
import com.google.cloud.translate.TranslateOptions;
import com.google.cloud.translate.Translation;

/**
 *
 * @author Burgeonits
 */
public class Google {
    public static void main(String args[]){
      
        
        
          Translate translate = TranslateOptions.getDefaultInstance().getService();
            String text = "Hello, world!";
              Translation translation =translate.translate(text,  TranslateOption.sourceLanguage("en"), TranslateOption.targetLanguage("ru"));
              
               System.out.printf("Text: %s%n", text);
                 System.out.printf("Translation: %s%n", translation.getTranslatedText());
    }
}
