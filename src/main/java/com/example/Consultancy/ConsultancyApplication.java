package com.example.Consultancy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@ComponentScan("com.example")
@EnableMongoRepositories("com.example.repository")
@SpringBootApplication
public class ConsultancyApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsultancyApplication.class, args);
	}
}
