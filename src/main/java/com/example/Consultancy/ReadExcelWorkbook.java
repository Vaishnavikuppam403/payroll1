/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.Consultancy;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Burgeonits
 */
public class ReadExcelWorkbook {
    
    public static void main(String args[]) throws FileNotFoundException, IOException{
        FileInputStream fileIn = null;
        FileOutputStream fileOut = null;
        XSSFWorkbook wb = null;
        
        try{
            fileIn= new FileInputStream("C:\\Users\\Burgeonits\\Desktop\\frontend\\Example.xlsx");
            //POIFSFileSystem fs = new POIFSFileSystem(fileIn);
            wb = new XSSFWorkbook(fileIn);
             Sheet datatypeSheet = wb.getSheetAt(0);
            Iterator<Row> iterator = datatypeSheet.iterator();

            while (iterator.hasNext()) {

                Row currentRow = iterator.next();
                Iterator<Cell> cellIterator = currentRow.iterator();

                while (cellIterator.hasNext()) {

                    while (cellIterator.hasNext()) {
            Cell cell = cellIterator.next();
            switch (cell.getCellType()) {
                case Cell.CELL_TYPE_NUMERIC:
                    System.out.print(cell.getNumericCellValue() + "(Integer)\t");
                    break;
                case Cell.CELL_TYPE_STRING:
                    System.out.print(cell.getStringCellValue() + "(String)\t");
                    
                    
                    break;
            }
        }
        System.out.println("");
    }
}
            
    }catch (IOException e) {
}
        
}
}
