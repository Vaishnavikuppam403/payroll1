/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.Consultancy;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFCell;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
/**
 *
 * @author Burgeonits
 */
public class Excelformula {
    public static void main(String args[])throws FileNotFoundException, IOException{
        XSSFWorkbook workbook = new XSSFWorkbook(); 
      XSSFSheet spreadsheet = workbook.createSheet("formula");
       XSSFRow row = spreadsheet.createRow(1);
       XSSFCell cell = row.createCell(0);
       cell.setCellValue("Monthly Earnings");
       cell=row.createCell(2);
       cell.setCellValue(10000);
         row = spreadsheet.createRow(3);
      cell = row.createCell(1);
      cell.setCellValue("Total = ");
      cell = row.createCell(2);
       row = spreadsheet.createRow(2);
         cell = row.createCell(1);
         
       cell.setCellType(XSSFCell.CELL_TYPE_FORMULA);
          cell.setCellFormula("ROUND((ROUND((TRUNC((3/13)*(C2+IF(ISNUMBER(SEARCH(\".33\",C2)),0.01,0)),0)+0.99)*(VLOOKUP((TRUNC((3/13)*(A5+IF(ISNUMBER(SEARCH(\".33\",C2)),0.01,0)),0)),LU_Scale2,2))-(VLOOKUP((TRUNC((3/13)*(C2+IF(ISNUMBER(SEARCH(\".33\",C2)),0.01,0)),0)),LU_Scale2,3)),0)*(13/3)),0)");
             cell = row.createCell(3);
//          cell.setCellValue("=ROUND((ROUND((TRUNC((3/13)*(C2+IF(ISNUMBER(SEARCH(\".33\",C2)),0.01,0)),0)+0.99)*(VLOOKUP((TRUNC((3/13)*(A5+IF(ISNUMBER(SEARCH(\".33\",C2)),0.01,0)),0)),LU_Scale2,2))-(VLOOKUP((TRUNC((3/13)*(C2+IF(ISNUMBER(SEARCH(\".33\",C2)),0.01,0)),0)),LU_Scale2,3)),0)*(13/3)),0)");
            row = spreadsheet.createRow(4);
FileOutputStream out = new FileOutputStream(new File("formula.xlsx"));
  workbook.write(out);
   System.out.println("fromula.xlsx written successfully");
      out.close();
     
    }
}
