/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.example.auspay.Auspay;
/**
 *
 * @author Burgeonits
 */
@Repository
public interface AusRepository extends MongoRepository<Auspay,Double>{
    
}
