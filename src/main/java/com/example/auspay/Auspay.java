/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.auspay;

import org.bson.types.Binary;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author Burgeonits
 */
@Document(collection = "auspay")
public class Auspay {
    
  @Id
    private Double monthlyearnings;
    private Double withtaxfree;
    private Double taxfree;

    public Double getMonthlyearnings() {
        return monthlyearnings;
    }

    public void setMonthlyearnings(Double monthlyearnings) {
        this.monthlyearnings = monthlyearnings;
    }

    public Double getWithtaxfree() {
        return withtaxfree;
    }

    public void setWithtaxfree(Double withtaxfree) {
        this.withtaxfree = withtaxfree;
    }

    public Double getTaxfree() {
        return taxfree;
    }

    public void setTaxfree(Double taxfree) {
        this.taxfree = taxfree;
    }
    private Binary fileupload;

    public Binary getFileupload() {
        return fileupload;
    }

    public void setFileupload(Binary fileupload) {
        this.fileupload = fileupload;
    }
}
      
    