/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.auspay;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.repository.AusRepository;
import com.example.upload.Upload;
import java.util.List;

/**
 *
 * @author Burgeonits
 */
@Service()
public class AusImpl implements AusService{
    
     @Autowired
    public AusRepository arep;
     
      @Override
    public Integer excel1(List<Auspay> List1) {
           System.out.println("file saving");
     
            for(Auspay u1:List1){
               // System.out.println("u1"+u1.getMonthlyearnings());
                arep.save(u1);
            }
                
    return 1;
}
    

    @Override
    public List<Auspay> findall() {
       
        List<Auspay> list=arep.findAll();
        
        return list;
        
    }
}
