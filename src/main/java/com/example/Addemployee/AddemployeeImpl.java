/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.Addemployee;

import com.example.repository.AddemployeeRepository;
import com.example.repository.TrailRepository;
import com.example.repository.RegistrationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.model.Registrationmodel;
import com.example.model.Trail;
import java.util.List;
import org.bson.types.Binary;

/**
 *
 * @author Burgeonits
 */
@Service()
public class AddemployeeImpl implements AddemployeeService{
     @Autowired
    public AddemployeeRepository addrep;

     @Autowired
     public RegistrationRepository rep;
   
     @Autowired 
     public TrailRepository trp;
     
    
     
     @Override
   public Addemployee addemployee(Addemployee add,String user) {
//        String user1=user;
       
        System.out.println("username"+user);
//         
       Registrationmodel reg=rep.findByUser(user);
       Trail tr= trp.findByUser(user);
       System.out.println(reg.getEmail());
      
        
       add.setUserid(user);
       System.out.println(reg.getFirstname());
      System.out.println("saving");
    
     System.out.println("add employee flag:"+reg.getFlag1());
     
     
     
      reg.setFlag1("second");
     //tr.setFlag2("second");
      //trp.save(tr);
      rep.save(reg);
         System.out.println("saved successfully"+reg);
       addrep.save(add);
      System.out.println(add.getUserid());
       return add;
    }

//
   

    
    @Override
    public Addemployee employee1(String user,String encodeToString) {
      
           System.out.println(encodeToString);  
         Addemployee ae=addrep.findByUserid(user);
         ae.setEncode(encodeToString);
         System.out.println(ae.getUserid());
          System.out.println(ae.getEncode());
          addrep.save(ae);
          System.out.println(ae.getEncode());
         return ae;
    }

    @Override
    public Addemployee imageupload(Binary data,String user,Integer age2) {
        
        Addemployee ad=addrep.findByUserid(user);
        ad.setAge(age2);
        ad.setFileupload(data);
        System.out.println(ad.getEncode());
         
       addrep.save(ad);
         return ad;
       
    }

    @Override
    public List<Addemployee> listemployee() {
       List<Addemployee> listemployee= addrep.findAll();
       
         return listemployee;
        
    }

   
}
