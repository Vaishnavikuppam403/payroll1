/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.Addemployee;

import com.example.login.LoginService;
import com.example.model.Registrationmodel;
import com.example.upload.Upload;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.bson.types.Binary;
import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
/**
 *
 * @author Burgeonits
 */
@Controller
public class AddemployeeController {
    @Autowired 
    public AddemployeeService addservice;
    
    @RequestMapping(value="basic1")
    public String addemployee(Model model,Addemployee add,String number,String location,HttpServletRequest req,HttpServletResponse res){
     
         HttpSession session = req.getSession();
        System.out.println("submitted values"+location);
        
       String user=(String) session.getAttribute("username");
       System.out.println("username"+user);
    
       
      Addemployee ad1=addservice.addemployee(add,user);
     
//        model.addAttribute(add);
        if(ad1!=null){
          
            System.out.println(ad1.getUserid());
            System.out.println("saved successfully");
            return "image";
        }
        
        return "addemployee";
    }
   
     
   @Value("${upload.path}")
    private String fileLocation;
@RequestMapping(value = "/imageupload1")
    public String imageupload(Model model,MultipartFile fileupload1,HttpServletRequest req,HttpServletResponse res)throws IOException, ParseException{
        HttpSession session = req.getSession();
      String user=(String) session.getAttribute("username"); 
   System.out.println(user);
     //System.out.println(add.getUserid());
         byte[] b=fileupload1.getBytes();
       
          Binary data=new Binary(b);
         System.out.println(b);
         Addemployee ad1=new Addemployee();
         ad1.setFileupload(data);
           String encodeToString = Base64.getEncoder().encodeToString(b);
         ad1.setEncode(encodeToString);
          System.out.println("data"+ad1.getEncode());
         System.out.println(user);
        System.out.println("upload"+ad1.getFileupload());
      
          File currDir = new File(".");
         String path = currDir.getAbsolutePath();
    fileLocation = path.substring(0, path.length() - 1) + fileupload1.getOriginalFilename();
    System.out.println(fileLocation);
    
    Addemployee ad2=addservice.employee1(user,encodeToString);
    String date=ad2.getDob();
    java.util.Date date1=new SimpleDateFormat("dd/MM/yyyy").parse(date);
        System.out.println(date1);
    System.out.println("dob"+ad2.getDob());
    Calendar dob = Calendar.getInstance();
dob.setTime(date1);
Calendar today = Calendar.getInstance();
int age2 = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR))
age2--;

System.out.println(age2);
   ad2.setAge(age2);
    System.out.println(ad2.getAge());
    if(ad2!=null){
         Addemployee str1=addservice.imageupload(data,user,age2);
         if(str1!=null){
              System.out.println(str1.getEncode());
             model.addAttribute("image",encodeToString);
             model.addAttribute("firstname", str1.getFname());
             model.addAttribute("lastname", str1.getLname());
            model.addAttribute("location", str1.getLocation());
            model.addAttribute("empid", str1.getEmpid());
            model.addAttribute("email", str1.getEmail1());
            model.addAttribute("phone", str1.getNumber());
             return "employeeprofile";
         }
         else{
           return "image";
         }
        
}
    else{
        return "loginsuccess";
    }
}
   @RequestMapping(value="newemployee") 
   public String newemployee(Model model){
    List<Addemployee> listemployee=addservice.listemployee();
     
    for(Addemployee addemp:listemployee){
    
//        byte[] b=(addemp.getFileupload()).getData();
//        String encodeToString = Base64.getEncoder().encodeToString(b);
        System.out.println("age"+addemp.getAge());
       System.out.println("encode"+addemp.getEncode());
        System.out.println("name"+addemp.getFname());
        System.out.println("image"+addemp.getFileupload());
        model.addAttribute("listemployee",listemployee);
//        model.addAttribute("image",encodeToString);
        
    }
        return "newemployee";
   }
    
}
        
    
